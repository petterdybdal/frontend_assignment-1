// Set default variables
let currentBalance = 200;
let pay = 0;
let currentLoan = 0;
let apiData;
const BASE_API_URL = 'https://hickory-quilled-actress.glitch.me/';

// Set variables for all elements we need to access
const outerLoan = document.getElementById('outer-loan');
const displayBalance = document.getElementById('balance');
const displayLoan = document.getElementById('loan');
const displayPay = document.getElementById('pay');
const payLoanButton = document.getElementById('pay-loan-button');
const select = document.getElementById('select-laptops');
const features = document.getElementById('features');
const laptopImage = document.getElementById('laptop-image');
const laptopHeader = document.getElementById('laptop-header');
const laptopDescription = document.getElementById('laptop-description');
const laptopPrice = document.getElementById('laptop-price');

// Initialize with default values and fetch API
window.onload = () => {
    updateCounts();
    getData();
};

// Update values
window.onclick = () => {
    updateCounts();
};

// Fetch data from API
const getData = async () => {
    try {
        await fetch(BASE_API_URL + 'computers')
            .then((response) => response.json())
            .then((data) => {
                apiData = data;
                // Populate features with id 0 as default
                populateFeatures(0);
                for (let key in data) {
                    // Create dropdown options
                    let option = document.createElement('option');
                    option.innerHTML = data[key].title;
                    option.value = key;
                    select.appendChild(option);
                }
            });
    } catch (e) {
        console.log(e);
    }
};

// Add eventlistener to dropdown
select.addEventListener('change', () => {
    populateFeatures(select.value);
});

// Populate information for the selected laptop
const populateFeatures = (id) => {
    features.innerHTML = '';
    laptopImage.src = BASE_API_URL + apiData[id].image;
    laptopImage.alt = apiData[id].title;
    const header = document.createElement('h2');
    header.innerHTML = 'Features';
    features.appendChild(header);
    const specs = apiData[id].specs;
    for (let i = 0; i < specs.length; i++) {
        const feature = document.createElement('p');
        feature.innerHTML = specs[i];
        features.appendChild(feature);
    }
    laptopPrice.innerText = formatPrice(apiData[id].price);
    laptopHeader.innerText = apiData[id].title;
    laptopDescription.innerHTML = apiData[id].description;
};

// Handles purchasing a computer
const purchaseComputer = () => {
    let active = select.value;
    let price = apiData[active].price;
    let title = apiData[active].title;
    console.log('buy computer ' + active + ' for ' + price);
    if (canPurchase(currentBalance, price)) {
        currentBalance -= price;
        alert(`Congratulations! You now own a ${title}.`);
    } else {
        alert("You can't afford that computer");
    }
};

const canPurchase = (balance, price) => {
    return balance >= price;
};

// Handles getting a loan
const getLoan = () => {
    let requestAmount = prompt('Enter amount for loan');
    if (
        isValidInput(requestAmount) &&
        isEligibleForLoan(currentBalance, requestAmount)
    ) {
        currentLoan = requestAmount;
        currentBalance += parseInt(requestAmount);
        outerLoan.style.display = 'flex';
        payLoanButton.style.display = 'block';
    } else {
        alert('Invalid amount!');
    }
};

// Validate and sanetize input
const isValidInput = (input) => {
    return !(isNaN(input) || input < 0);
};

// Check if eligible for loan
const isEligibleForLoan = (amount, requestAmount) => {
    if (!isNaN(requestAmount) && requestAmount > 0) {
        return requestAmount <= amount * 2 && currentLoan === 0;
    }
};

const doWork = () => {
    pay += 100;
};

// Method for depositing pay into balance
const bank = () => {
    // Check if has loan
    if (currentLoan > 0) {
        // Check if paying 10% to loan would make currentLoan negative
        if (pay * 0.1 < currentLoan) {
            // All good, subtract 10%
            currentLoan -= pay * 0.1;
            pay *= 0.9;
        } else {
            // Loan will be negative, subtract loan from pay and set loan to 0
            pay -= currentLoan;
            currentLoan = 0;
            payLoanButton.style.display = 'none';
            outerLoan.style.display = 'none';
        }
    }
    currentBalance += pay;
    pay = 0;
};

// Updates the DOM elements to display the data stored in js
const updateCounts = () => {
    displayLoan.innerText = formatPrice(currentLoan);
    displayPay.innerText = formatPrice(pay);
    displayBalance.innerText = formatPrice(currentBalance);
};

const formatPrice = (price) => {
    return price + ' NOK';
};

const payLoan = () => {
    // Check if loan will be payed off or not
    if (currentLoan > pay) {
        currentLoan -= pay;
        pay = 0;
    } else {
        pay -= currentLoan;
        currentLoan = 0;
        bank();
        payLoanButton.style.display = 'none';
        outerLoan.style.display = 'none';
    }
};
