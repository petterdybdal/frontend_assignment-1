# Banking application

## Description

A simple banking application for the first assignment in front-end part of the Noroff fullstack-.NET course.

The application has interfaces to simulate working, banking, taking loans and purchasing computers.

## Installation

Simply git clone the repository. The index.html file needs to be hosted somehow. In the project I used an extension for Visual Studio Code called Live Server to solve this, but it can be hosted through other services as well.

## Interface

### Bank

The user can view their balance, and take a loan. The loan can not exceed 2 times the users balance, and the user can not take a loan if they already have one.

### Work

The user can press the "work"-button to generate 100 NOK. This currency can be banked, or be used to pay an outstanding loan if there is one. If the user banks the money with an outstanding loan, 10% if it will be used to pay down the loan automatically.

### Computer

The user can view and choose different computers from the drop-down. The features and the price of the computer will be displayed when the user chooses a computer. This data is fetched from an external API, along with the images for each of the computers.

If the user wishes to, they can also purchase a computer if they have the funds for it. Purchasing a computer doesn't do anything other than subtracting the price from the users balance.

## Contributors

The application is developed by Petter Dybdal.
